export class SleepData {
  Awakening:boolean;
  Medication:boolean;
  BedTime:Time;
  WakeTime:Time;
  AwakeningTime:Time[];
  AwakenedTime:number[];
  
}

export class Exercise{
  Title : string;
  StartTime : Time;
  EndTime : Time;
  Details : string;
  Distance : number;
}

export class Time{
  Hours:number;
  Minutes:number;
  constructor(){};
}
