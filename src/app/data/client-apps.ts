import { ClientApp } from '../models/client-app';
/**
 * List of SMART on FHIR Client Applications defined in the SAMPLE APPLICATION
 */
export const CLIENT_APPS: ClientApp[] = [
    {
        name: 'Health',
        uniqueName: 'chbaseppe',
        clientId: '2c099818-5ee3-4ec9-8cc6-fc27bd1d20b6',
        redirectUri: 'http://localhost:4201/redirect/',
        launchUrl: 'http://localhost:4201/redirect/',
        scopes: 'patient/*.*',
        standalonePatient: true,
        ehrLaunch: true,
        server: 'chbaseppe',
        secret: '7611a4a3-4f1e-4f3d-8d62-c0f7c5ce56f6'
    }
];
