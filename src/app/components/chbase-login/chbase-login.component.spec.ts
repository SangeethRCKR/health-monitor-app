import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChbaseLoginComponent } from './chbase-login.component';

describe('ChbaseLoginComponent', () => {
  let component: ChbaseLoginComponent;
  let fixture: ComponentFixture<ChbaseLoginComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChbaseLoginComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChbaseLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
