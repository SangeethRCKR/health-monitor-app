import { Component, OnInit } from '@angular/core';
import { $ } from 'protractor';
import { SleepData, Exercise } from '../../models/HealthData';
import { ObservationsService } from '../../services/observations.service'


@Component({
  selector: 'app-daily-data',
  templateUrl: './daily-data.component.html',
  styleUrls: ['./daily-data.component.scss']
})
export class DailyDataComponent implements OnInit {

  constructor(private obs: ObservationsService) { }

  ngOnInit(): void {
  }

  exercise: Exercise = new Exercise();
  sleepdata: SleepData = new SleepData();
  showAdditionalInfo: number;
  formPage: number = 1;
  Arr = Array;
  noOfAwakenings: number = 1;
  noOfMedications: number = 1;
  body:string ;
  weight : number = 0;

  submitDailyData() {
    this.obs.getObservation().subscribe(res => { console.log(res) });
    console.log(this.weight);
  }

  submitWeight(){
    this.obs.postWeight(this.postBody()).subscribe();
  }

  ifDistance() {
    if (this.exercise.Title == "running" || this.exercise.Title == "swimming") {
      this.showAdditionalInfo = 1;
    }
    else if (this.exercise.Title == "other") {
      this.showAdditionalInfo = 2;
    }
    else {
      this.showAdditionalInfo = 0;
    }
  }


  postBody() {
    var result = JSON.stringify(
      {
        "resourceType": "Observation",
        "status": "final",
        "category": [
          {
            "coding": [
              {
                "system": "http://hl7.org/fhir/observation-category",
                "code": "vital-signs",
                "display": "Vital Signs"
              }
            ],
            "text": "Vital Signs"
          }
        ],
        "code": {
          "coding": [
            {
              "system": "https://fhir.chbase.com/fhir/stu3/ValueSet/wc/vital-statistics",
              "version": "1",
              "code": "wgt",
              "display": "Body Weight"
            }
          ]
        },
        "subject": {
          "reference": "Patient/ef08a396-1fc4-4b23-8bcc-fe8d892378a2"
        },
        "effectiveDateTime": "2018-07-10T18:50:00-04:00",
        "issued": "2020-07-10T22:55:16.784+00:00",
        "valueQuantity": {
          "value": this.weight,
          "unit": "kg",
          "system": "http://unitsofmeasure.org/",
          "code": "kg"
        }
      }

    );

    var json = JSON.parse(result);

    return json;

  }

}
