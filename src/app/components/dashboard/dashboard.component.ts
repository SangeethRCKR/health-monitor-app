import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {SmartService,AuthService} from '../../services';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(private router:Router,private smartService:SmartService,private authService:AuthService) { }

  ngOnInit(): void {

    this.redirectToLogin();
  }
  storage :any;
  
  logout()
  {
    this.authService.logout();
  }
  redirectToLogin()
  {
    const tokenResponse = sessionStorage.getItem('tokenResponse');
    if(tokenResponse == null)
    {
      this.router.navigate(['']);
    }
  }

}
