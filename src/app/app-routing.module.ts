import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import {ChbaseLoginComponent} from './components/chbase-login/chbase-login.component';
import {RedirectComponent} from '../app/redirect/redirect.component';

const routes: Routes = [
  {path:'login',component:ChbaseLoginComponent},
  {path:'redirect',component:RedirectComponent},
  {path:'dashboard',component : DashboardComponent}, 
  {path:'',redirectTo:'/login',pathMatch:'full'}, 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
