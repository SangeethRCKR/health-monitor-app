import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import {MatExpansionModule} from '@angular/material/expansion';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule ,HTTP_INTERCEPTORS} from '@angular/common/http';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTabsModule } from '@angular/material/tabs';
import { MatIconModule } from '@angular/material/icon'
import { MatCardModule } from '@angular/material/card';
import { SummaryComponent } from './components/summary/summary.component';
import { DailyDataComponent } from './components/daily-data/daily-data.component';
import { SetGoalsComponent } from './components/set-goals/set-goals.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ChbaseLoginComponent } from './components/chbase-login/chbase-login.component';
import {HttpInterceptorService} from './services/http-interceptor.service';


@NgModule({
  declarations: [
    AppComponent,
    SummaryComponent,
    DailyDataComponent,
    SetGoalsComponent,
    DashboardComponent,
    ChbaseLoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatTabsModule,
    MatIconModule,
    MatCardModule,
    MatExpansionModule,
    FormsModule,
    HttpClientModule,


  ],
  providers: [ {
    provide: HTTP_INTERCEPTORS,
    useClass: HttpInterceptorService,
    multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
