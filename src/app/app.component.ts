import { Component,OnInit,OnDestroy } from '@angular/core';
import { Subject,Observable } from 'rxjs';
import { ClientAppService, FhirServerService, GlobalService } from '../app/services';
import {takeUntil} from 'rxjs/operators';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'healthmonitor-app';
//   url :String;
//   app: any;
//   server: any;
//   scopes: any;
//   accessType = 'online_access';
//   additionalScopes = '';

//   private _unsubscribe = new Subject<void>();

//   constructor(private _clientAppService: ClientAppService,
//     private _fhirServerService: FhirServerService,
//     private _globalService: GlobalService) { }

//  ngOnInit() {
//     this._clientAppService.getAllClientApps()
//       .subscribe(apps => {
//         console.log(apps);
//         const uniqueName ='chbaseppe'; 
//         //this._route.snapshot.paramMap.get('uniqueName');
//         // Get the app based on the route parameter
//         this.app = apps.find(q => q.uniqueName === uniqueName);
//         const scopesString: string = this.app.scopes;
//         const scopesArray = scopesString.split(',');
//         const scopes = scopesArray.map(scope => {
//           return { value: scope.trim(), checked: true };
//         });
//         // Add the default scopes requried for a patient standalone launch
//         scopes.push({ value: 'launch/patient', checked: true });
//         scopes.push({ value: 'openid', checked: true });
//         scopes.push({ value: 'profile', checked: true });
//         this.scopes = scopes;
//         const serverUniqueName = this.app.server;
//         this._fhirServerService.getServer(serverUniqueName)
//           .subscribe(server => {
//             this.server = server;
//           });
//       });
    
//   }
 
//   connect() {
   
//     let selectedScopes = this.scopes.filter(q => q.checked === true).map(v => v.value).join(' ');
//     if (this.server.supportsAccessTypes === true) {
//       selectedScopes = selectedScopes + ' ' + this.accessType;
//     }
//     if (this.additionalScopes !== '') {
//       selectedScopes = selectedScopes + ' ' + this.additionalScopes;
//     }
//     const clientSettings: FHIR.SMART.OAuth2ClientSettings = {
//       client_id: this.app.clientId,
//       scope: selectedScopes,
//       redirect_uri: this.app.redirectUri,
//       state: this.app.uniqueName,
//     }; 
//     if (this.app.secret) {
//       clientSettings.secret = this.app.secret;
//     }
//     const oauth2Configuration: FHIR.SMART.OAuth2Configuration = {
//       client: clientSettings,
//       server: this.server.baseUrl
//     };
//     FHIR.oauth2.authorize(oauth2Configuration, (authError) => {
//       this._globalService.setError(authError);
//     });
//   }
}
