import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ObservationsService {

  constructor(private http: HttpClient) { }

  getObservation(): Observable<JSON> {
    const storage=JSON. parse(sessionStorage.getItem('tokenResponse'));
    const url=`http://platform.dev.grcdev.com/api/fhir/Observation?patient=${storage.patient}`;
    return this.http.get<JSON>(url);
  }

  postWeight(body:JSON){
    const storage=JSON. parse(sessionStorage.getItem('tokenResponse'));
    const url=`http://platform.dev.grcdev.com/api/fhir/Observation?patient=${storage.patient}`;
    return this.http.post(url,body);
  }
}
